import { MainPage } from "./Pages/Main";
import {Header} from "./Components/Header";
import { Route, Routes } from "react-router-dom";
import {SignInPage} from "./Pages/SignIn";
import { SignUpPage } from "./Pages/SignUp";
import { useState } from "react";
import { TestPage } from "./Pages/Test";
import { ProductsPage } from "./Pages/Products";

function App() {
    const [isAuth, setIsAuth] = useState(false)
    return (
    <div className="App" style={{margin: '0', padding: '0'}}>
        <Header/>
        <Routes>
            <Route path="/" exact element={<MainPage auth={isAuth} setIsAuth={setIsAuth}/>}/>
            <Route path="/signin" element={<SignInPage auth={isAuth} setIsAuth={setIsAuth}/>}/>
            <Route path="/test" element={<TestPage/>}/>
            <Route path="/products" element={<ProductsPage/>}/>
            <Route path="/signup" element={<SignUpPage/>}/>
        </Routes>
    </div>
    );
}

export default App;
