import { SignIn } from "../../Components/Registration/SignIn";

export const SignInPage = ({auth, setIsAuth}) => {
    return (
        <>
            <SignIn auth={auth} setIsAuth={setIsAuth}/>
        </>

    )
}