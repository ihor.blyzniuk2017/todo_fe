import { Todo } from "../../Components/Todo";

export const MainPage = ({auth, setIsAuth}) => {
    return (
        <>
            <Todo auth={auth} setIsAuth={setIsAuth}/>
        </>

    )
}