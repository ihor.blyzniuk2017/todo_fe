import { useEffect, useState } from "react";
import { Wrapper } from "../styled";
import axios from "axios";

export const Todo = ({auth, setIsAuth}) => {
    const [todosData, setTodosData] = useState([])
    const [showInput, setShowInput] = useState(false)
    const [inputData, setInputData] = useState('')
    const [idEdit, setIdEdit] = useState(false)
    useEffect( () => {
        axios.get('http://localhost:8000/api/todos')
            .then((response) => setTodosData(response.data.data))
    }, [])

    const createTodo = async () => {
        const urlEdit = idEdit ? `/${idEdit}` : ''
        console.log(inputData)
        const body = {"title": inputData}
        await axios.post(`http://localhost:8000/api/todos${urlEdit}`, body)
            .then((response) => {
                setTodosData(response.data.data)
                setShowInput(false)
                setInputData('')
                setIdEdit(false)
            })
    }
    console.log('todosData',todosData)

    const deleteTodo = async (id) => {
        await axios.delete(`http://localhost:8000/api/todos/${id}`)
            .then((response) => setTodosData(response.data.data))
    }

    const editTodo = async (id , title) => {
        setShowInput(true)
        setInputData(title)
        setIdEdit(id)
    }

    return (
        <Wrapper>
            <div>
                <button onClick={() => setShowInput(true)}>Add todo</button>
            </div>
            {
                showInput && <div>
                    <input value={inputData} onChange={(e) => setInputData(e.target.value)}/>
                    <button onClick={createTodo}>save</button>
                </div>
            }
            {todosData.length ? todosData.map((item, index) => (
                <ul key={index}>
                    <li>
                        {item.title}
                        <button onClick={() => editTodo(item.id, item.title)}>edit</button>
                        <button onClick={() => deleteTodo(item.id)}>delete</button>
                    </li>
                </ul>
            )) : <p>you have no stuff to do</p>}
        </Wrapper>
    )
}