import styled from "styled-components";
import { Link } from "react-router-dom";

export const Title = styled.h2`
  text-align: center;
`

export const Container = styled.div`
  display: block;
  width: 400px;
  margin: 0 auto;
  text-align: center;
`

export const Input = styled.input`
  display: block;
  margin: 0 auto 15px;
  padding: 0 7px;
  width: 300px;
  height: 30px;
  font-size: 15px;
`

export const Btn = styled.button`
  display: block;
  margin: 0 auto;
  width: 100px;
  height: 30px;
`
export const LinkItem = styled(Link)`
  margin-top: 20px;
  color: #000;
  text-decoration: none;
  &:hover {
    text-decoration: none;
    color: #000;
  }
`