import { Wrapper } from "../../styled";
import { Btn, Input, Container, Title } from "../styled";
import { LinkItem } from "../styled";
import { useState } from "react";
import axios from "axios";

export const SignUp = ({auth, setIsAuth}) => {
    const [formData, setFormData] = useState({
        name: '',
        email: '',
        password: ''
    })

    const emailHandler = (e) => {
        setFormData({
            ...formData,
            email: e.target.value
        })
    }
    const passwordHandler = (e) => {
        setFormData({
            ...formData,
            password: e.target.value
        })
    }
    const nameHandler = (e) => {
        setFormData({
            ...formData,
            name: e.target.value
        })
    }


    const onClickHandler = async () => {
        await axios.post('http://localhost:8000/api/register', formData)
            .then((response) => {
                console.log(response.data)
            })
    }
    return (
        <Wrapper>
            <Title>Sign up</Title>
            <Container>
                <Input placeholder='name' onChange={(e) => nameHandler(e)}/>
                <Input placeholder='email' onChange={(e) => emailHandler(e)}/>
                <Input type='password' placeholder='password' onChange={(e) => passwordHandler(e)}/>
                <Btn onClick={onClickHandler}>sign up</Btn>
                <LinkItem to='/signin'>signin</LinkItem>
            </Container>
        </Wrapper>
    )
}