import { Wrapper } from "../../styled";
import { Btn, Input, Container, Title } from "../styled";
import { LinkItem } from "../styled";
import { useState } from "react";
import axios from "axios";

export const SignIn = ({auth, setIsAuth}) => {
    const [formData, setFormData] = useState({
        email: '',
        password: ''
    })

    const emailHandler = (e) => {
        setFormData({
            ...formData,
            email: e.target.value
        })
    }
    const passwordHandler = (e) => {
        setFormData({
            ...formData,
            password: e.target.value
        })
    }


    const onClickHandler = async () => {
        await axios.post('http://localhost:8000/api/login', formData)
            .then((response) => {
                console.log(response)
            })
    }
    return (
        <Wrapper>
            <Title>Sign in</Title>
            <Container>
                <Input placeholder='email' onChange={(e) => emailHandler(e)}/>
                <Input type='password' placeholder='password' onChange={(e) => passwordHandler(e)}/>
                <Btn onClick={onClickHandler}>sign in</Btn>
                <LinkItem to='/signup'>signup</LinkItem>
            </Container>
        </Wrapper>
    )
}