import { Wrapper } from "../styled";
import { useEffect, useState } from "react";
import axios from "axios";
import { apiClient } from "../../services/api";

export const Test = () => {
    const [dataBooks, setDataBooks] = useState([])
    const [dataCinemas, setDataCinemas] = useState([])
    const [dataMovies, setDataMovies] = useState([])
    const [dataFilms, setDataFilms] = useState([])
    const [dataAuthors, setDataAuthors] = useState([])


    // useEffect(() => {
    //     axios.get('http://localhost:8000/api/films')
    //         .then((response) => {
    //             setDataFilms(response.data)
    //         })
    //     axios.get('http://localhost:8000/api/authors')
    //         .then((response) => {
    //             setDataAuthors(response.data)
    //         })
    // }, [])

    const getFilms = async () => {
        await apiClient.get('films')
            .then((response) => {
                setDataFilms(response.data)
                setDataAuthors([])
                setDataBooks([])
                setDataCinemas([])
                setDataMovies([])
            })
    }
    const getBooks = async () => {
        await apiClient.get('books')
            .then((response) => {
                setDataBooks(response.data)
                setDataFilms([])
                setDataAuthors([])
                setDataCinemas([])
                setDataMovies([])
            })
    }
    const getAuthors = async () => {
        await apiClient.get('authors')
            .then((response) => {
                setDataAuthors(response.data)
                setDataFilms([])
                setDataBooks([])
                setDataCinemas([])
                setDataMovies([])
            })
    }

    const getCinemas = async () => {
        await apiClient.get('cinemas')
            .then((response) => {
                setDataCinemas(response.data)
                setDataFilms([])
                setDataBooks([])
                setDataAuthors([])
                setDataMovies([])
            })
    }

    const getMovies = async () => {
        await apiClient.get('movies')
            .then((response) => {
                setDataMovies(response.data)
                setDataCinemas([])
                setDataFilms([])
                setDataBooks([])
                setDataAuthors([])
            })
    }

    return (
        <Wrapper>
            <div>
                <button onClick={getFilms}>Get films</button>
                <button onClick={getBooks}>Get books</button>
                <button onClick={getAuthors}>Get authors</button>
                <button onClick={getCinemas}>Get cinemas</button>
                <button onClick={getMovies}>Get movies</button>
            </div>

            {dataFilms.length ? dataFilms.map((item, index) => {
                return (
                    <>
                        <h2>Films</h2>
                        <div key={index}>{item.name} <br/> director: {item.director}<hr/></div>
                    </>
                )
            }) : null}
            {dataBooks.length ? dataBooks.map((item, index) => {
                return (
                    <>
                        <h2>Films</h2>
                        <div key={index}>{item.title} <br/> author: {item.author}<hr/></div>
                    </>
                )
            }) : null}
            {dataAuthors.length ? dataAuthors.map((item, index) => {
                return (
                    <>
                        <h2>Books</h2>
                        <div ><h4>{item.name}</h4> <br/> <b>books:</b> {Object.values(item.books).map((item) => {return(<p>{item}</p>)})}<hr/></div>
                    </>

                )
            }) : null}
            {dataCinemas.length ? dataCinemas.map((item) => {
                return (
                    <>
                        <h2>Cinemas</h2>
                        <div><h4>{item.name}</h4><br/> <b>movies:</b> {Object.values(item.movies).map((item) => {return(<p>{item.name}</p>)})}<hr/></div>
                    </>
                )
            }): null}

            {dataMovies.length ? dataMovies.map((item) => {
                return (
                    <>
                        <h2>Movies</h2>
                        <div><h4>{item.name}</h4><br/> <b>cinemas:</b> {Object.values(item.cinemas).map((item) => {return(<p>{item.name}</p>)})}<hr/></div>
                    </>
                )
            }): null}
        </Wrapper>
    )
}