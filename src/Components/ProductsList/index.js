import { Wrapper } from "../styled";
import { useState } from "react";
import { apiClient } from "../../services/api";
import { Img, Left, Li, Right } from "./styled";

export const ProductsList = () => {
    const [inputValue, setInputValue] = useState('')
    const [productsData, setProductsData] = useState([])
    console.log(productsData)
    const [selected, setSelected] = useState([])
    const searchHandler = async () => {
        await apiClient.get(`/ofdb/cgi/search.pl?search_terms=${inputValue}&search_simple=1&action=process&json=1`)
            .then((response) => {
                setProductsData(response.data.products)
            })
    }

    const addToSelected = (item) => {
        setSelected(prev => {
            return [...prev, item]
        })
    }

    const onItemDelete = (id) => {
        const result = selected.filter((product) => product._id !== id)
        setSelected(result)
    }

    return (
        <Wrapper>
            <div>
                <input value={inputValue} onChange={(e) => setInputValue(e.target.value)}/>
                <button onClick={searchHandler}>search</button>
                {productsData.length ? productsData.map((item, index) => {
                    return (
                        <ul>
                            <Li>
                                <Left>
                                    <Img alt='product' src={item.image_url}/>
                                </Left>
                                <Right>
                                    <h3>product name: {item.product_name || item.product_name_en || item.product_name_fr}</h3>
                                    <table>
                                        <thead>
                                        <tr>
                                            <th>Carbs</th>
                                            <th>Fat</th>
                                            <th>Proteins</th>
                                            <th>Sodium</th>
                                            <th>Sugars</th>
                                            <th>Calories(100g)</th>
                                            <th>Calories(serving)</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{item.nutriments.carbohydrates}</td>
                                            <td>{item.nutriments.fat}</td>
                                            <td>{item.nutriments.proteins}</td>
                                            <td>{item.nutriments.sodium}</td>
                                            <td>{item.nutriments.sugars}</td>
                                            {/*<td>{item.nutriments.energy-kcal}</td>*/}
                                            {/*<td>{item.nutriments.energy-kcal_serving}</td>*/}
                                        </tr>

                                        </tbody>
                                    </table>
                                    <button onClick={() => addToSelected(item)}>add</button>
                                </Right>
                            </Li>
                            <hr/>
                        </ul>
                    )
                }) : null}
            </div>
            <hr/>
            <hr/>
            <div>
                {selected.length ? selected.map((item, index) => {

                    return (
                        <>
                            <h3>product name: {item.product_name || item.product_name_en || item.product_name_fr}</h3>
                            <table>
                                <thead>
                                <tr>
                                    <th>Carbs</th>
                                    <th>Fat</th>
                                    <th>Proteins</th>
                                    <th>Sodium</th>
                                    <th>Sugars</th>
                                    <th>Calories(100g)</th>
                                    <th>Calories(serving)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{item.nutriments.carbohydrates}</td>
                                    <td>{item.nutriments.fat}</td>
                                    <td>{item.nutriments.proteins}</td>
                                    <td>{item.nutriments.sodium}</td>
                                    <td>{item.nutriments.sugars}</td>
                                </tr>

                                </tbody>
                            </table>
                            <button onClick={() => onItemDelete(item._id)}>delete</button>
                        </>
                    )
                }) : <p>you did not select food</p>}

            </div>
        </Wrapper>
    )
}