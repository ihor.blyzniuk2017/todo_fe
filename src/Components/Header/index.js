import { HeaderContainer, LinkItem, ListItem, NavList } from "./styled";

export const Header = () => {
    return (
        <HeaderContainer>
            <NavList>
                <ListItem>
                    <LinkItem to='/'>Main</LinkItem>
                </ListItem>
                <ListItem>
                    <LinkItem to='/test'>Test</LinkItem>
                </ListItem>
                <ListItem>
                    <LinkItem to='/products'>Products</LinkItem>
                </ListItem>
                <ListItem>
                    <LinkItem to='/signin'>Log in</LinkItem>
                </ListItem>
            </NavList>
        </HeaderContainer>
    )
}