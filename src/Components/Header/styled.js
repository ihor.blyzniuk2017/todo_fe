import styled from "styled-components";
import {Link} from "react-router-dom";

export const HeaderContainer = styled.header`
  background-color: darkgray;
  margin: 0;
  padding: 15px 30px;
`
export const NavList = styled.ul`
  margin: 0;
  padding: 0;
  display: flex;
  justify-content: space-between;
`
export const ListItem = styled.li`
  list-style-type: none;
`
export const LinkItem = styled(Link)`
  color: #000;
  text-decoration: none;
  &:hover {
    text-decoration: none;
    color: #000;
  }
`